package agh.qa;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Code2Tests {
    @Test
    public void TestAGreaterThen0LessThe10(){
        Code2 code2 = new Code2();
        int a = 11;
        int result = code2.calculate(a);

        Assert.assertEquals(result, a);
    }
}
